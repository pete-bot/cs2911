import java.util.ArrayList;

/**
* @author Peter Kydd	
* 		z3367463
* 
*  this class stores the current state of the search
*  cost so far and the f/g scores are also stored. 
*  the current city is stored along with the list of connections called visisted - this is used in our search algorithm
* 
*/

public class State implements Comparable<State>{
	
	/**
	 * Class constructor
	 * @param newCity
	 * 		newCity is the 'current' city of the node
	 */
	public State(String newCity){
		gScore = 0;
		fScore = 0;
		currentCity = newCity;
		visited = new ArrayList<Connection>();
	}
	
	
	/**Compare the list of visited flights with the list of required flights
	 * @param requiredFlights
	 * 		the required flights list
	 * @return
	 * 		true if all required flights have been flown. false otherwise
	 */
	public boolean goalState(ArrayList<Connection> requiredFlights){
		
		if( (visited== null) && (requiredFlights == null) ){
			return true;
		}
		if ( (visited == null) ){
			System.out.println("visited == NULL");
			return false;
		}
		
		// early exit if fewer visited flights than required
		// since the visited list must be AT LEAST as large as the required list
		// for it to contain all of the required flights
		if(visited.size()<requiredFlights.size()){
			return false;
		}
		
		// check if the visited list contains the required flights
		for (Connection c : requiredFlights){
			if(!visited.contains(c)){
				return false;
			}
		}
		
		return true;
		
		
	}


	/**
	 * @return
	 * 		String, the current city
	 */
	public String getCurrentCity(){
		return currentCity;
	}
	
	
	/**Set the Gscore
	 * @param newScore
	 * 		the score that will be used to update the Gscore.
	 */
	public void setGScore(int newScore){
		gScore = newScore;
	}
	
	
	/**
	 * @return
	 * 		the Gscore of this state
	 */
	public int getGScore(){
		return gScore;
	}
		
	
	/**
	 * @param newScore
	 * 		The value to set the FScore
	 */
	public void setFScore(int newScore){
		fScore = newScore;
	}
	
	/**
	 * @return
	 * 		the current F score
	 */
	public int getFScore(){
		return fScore;
	}
	
	/* (non-Javadoc)
	 * Modified compareTo method
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(State compState) {
		return (fScore-compState.getFScore() );
	}
	
	// set the visited array list
	/** Set the visited arraylist
	 * @param newVisited
	 * 		the new visited arrayList (usually from the predecessor state)
	 */
	public void setVisited(ArrayList<Connection> newVisited){
		visited = newVisited;
	}

	
	/** Produce a clone of the current visited array List
	 * @return
	 * 		the cloned visited ArrayList
	 */		
	public ArrayList<Connection> getVisitedClone(){
	    ArrayList<Connection> visitedClone = new ArrayList<Connection>(visited.size());
	    for(Connection element : visited){
	    	Connection connectionClone = (Connection) element.clone();
	    	visitedClone.add(connectionClone);
	    }
	    return visitedClone;
	}
	
	
	/**
	 * @param newConnection
	 * 		new connection to add to the visited list
	 */
	public void addVisisted(Connection newConnection){
		visited.add(newConnection);
	}
	
	
	/**
	 * show path - used for debugging
	 */
	public void showPath(){
		for(Connection c : visited){
			c.showConnection();
		}
	}
	
	private int gScore;
	private int fScore;
	private String currentCity;
	private ArrayList<Connection> visited;
	
}