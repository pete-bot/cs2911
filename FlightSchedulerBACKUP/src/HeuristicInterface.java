import java.util.ArrayList;
import java.util.Hashtable;

/**
 * @author Peter Kydd
 * 		z3367463
 */
public interface HeuristicInterface {	
	/**
	 * @param successor
	 * 		The state that is currently being passed into the heuristic.
	 * @param connections
	 * 		The list of all connections in the state space
	 * @param newCities
	 * 		The hash of city weights that are available
	 * @return
	 * 		The return value of this function is the hScore that will be applied during the A* algo,
	 * 		to that particular state.
	 */
	int getCostEstimate(State successor, ArrayList<Connection> connections,  Hashtable<String, Integer> newCities);
	
}
