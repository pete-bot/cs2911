import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.PriorityQueue;

/**
 * @author Peter Kydd
 * 	z3367463
 *	 COMP2911 FlightScheduler 
 *	this is the main class - it it will take in arguments from the command line 
 *	It also creates the objects that will be used for the A* search, and interface the heuristic and A*
 *
 */

public class FlightScheduler {

	// constructor 
	public FlightScheduler(){
		cities = new Hashtable<String, Integer>();
		connections = new ArrayList<Connection>();
		reqFlights = new ArrayList<Connection>();
	}
		
    public static void main(String args[]){ 
		
		// create a new flightScheduler object
		// parse our input
    	FlightScheduler flightManifest = new FlightScheduler();
		flightManifest.parseInput(args);
		
		Heuristic balancedHeuristic = new Heuristic();
		flightManifest.aStar(balancedHeuristic);
		
	}
	
	public void parseInput(String args[]){
		// scanner implements the iterator for strings
				Scanner sc = null;
				try	{
				    sc = new Scanner(new FileReader(args[0]));
				    while(sc.hasNext()){
				    	String line = sc.next();
				    	
				    	// create a new 'city', give it an index and a weight value
				    	if(line.equals("City")){
				    		String cityName = sc.next();
				    		int cityDelay = Integer.parseInt(sc.next());
				    		// create a new city node here and pass that object through to the graph
				    		this.addCity(cityName, cityDelay);

				    		
				    	// add connections to the graph and update the edge weight
				    	} else if(line.equals("Time")){
				    		String cityOne = sc.next();
				    		String cityTwo = sc.next();
				    		int pathWeight = Integer.parseInt(sc.next());
				    		
				    		Connection newConnection = new Connection(cityOne, cityTwo, pathWeight);
				    		this.addConnection(newConnection);
				    		
				    		// create a corresponding connection in the other direction
				    		Connection newConnectionReverse = new Connection(cityTwo, cityOne, pathWeight);
				    		this.addConnection(newConnectionReverse);
				    			
				    	// queue up the flights for our goal state
				    	} else if(line.equals("Flight")){
				    		String cityOne = sc.next();
				    		String cityTwo = sc.next();
				    	
				    		// add a required flight to the list
				    		//Connection newFlight = new Connection(cityOne, cityTwo);
				    		Connection tempConnection = new Connection(cityOne, cityTwo);
				    		for(Connection searchConnection : this.getconnectionsClone()){
				    			if(searchConnection.equals(tempConnection)){
				    				tempConnection = searchConnection;
				    				break;
				    			}
				    		}
				    		this.addRequiredFlights(tempConnection);
				    	}
				    }
				}
				
				catch (FileNotFoundException e) {
					System.out.println("No arguments found.");
				}
				
				finally
				{
					if (sc != null) sc.close();
				}
		
		
		return;
	}
	
	
	/**
	 * addCity
	 * @param newCity
	 * 		Name of new city entry to be created in hash table
	 * @param weight
	 * 		Weight or cost of the city. (delay time)
	 */
	public void addCity(String newCity, int weight){
		cities.put(newCity, weight);
	}
		
	/**
	 * addConnection
	 * @param newConnection
	 * 		Connection that will be added to the FlightManager class connection ArrayList
	 */		
	public void addConnection(Connection newConnection){
		connections.add(newConnection);
	}
	
	/**addRequiredFlights
	 * @param newConnection
	 * 		Connection that will be added to the required flights ArrayList
	 */
	public void addRequiredFlights(Connection newConnection){
		reqFlights.add(newConnection);
	}
	
	/**A* search algorithm
	 * @param heuristic
	 * 		the heuristic strategy that will be used in this algorithm
	 */
	public void aStar(Heuristic heuristic){
		
		// our priority queue for the 'open states'. This is where the 
		PriorityQueue<State> frontier = new PriorityQueue<State>();	// OPEN SET representation
		
		// create initial state. This state is always located at SYDNEY. we DO NOT include the sydney delay time.
		int expandedCities = 0;
		int initialScore = 0;
		State initialState = new State("Sydney");
		initialState.setGScore(initialScore);
		initialState.setFScore(initialState.getGScore()+heuristic.getCostEstimate(initialState, reqFlights, cities));
		
		// create a current state and add to frontier
		State current;
		frontier.add(initialState);
		
		// while our priority queue still has states
		while (!frontier.isEmpty()){
			// PQ uses the comparator implementation in State class	
			
			// pop the first element from the priority queue and call it 'current'
			current = frontier.poll();
	
			// check goal state - if goal state, print out the result.
			if(current.goalState(reqFlights)){
				System.out.println(expandedCities+" nodes expanded");
				
				// remove delay of last city - we do not consider this delay
				current.setGScore(current.getGScore()-cities.get(current.getCurrentCity()));
				System.out.println("cost = "+current.getGScore());
				current.showPath();
				return;
			}
			
			// increment our expanded city count
			expandedCities++;
			
			State next;
			
			// generate successor states
			// iterate through our neighbor cities and create a successor state
			for(Connection tempConnection: connections){
		
				if((current.getCurrentCity()).equals(tempConnection.getOrigin())){
					next = new State(tempConnection.getDestination());
					
					// clone the 'visited' array list of the previous state - we will update and add the current connection to this
					next.setVisited(current.getVisitedClone());
					next.addVisisted(tempConnection);
					
					// calculate and populate our G score then F score.
					next.setGScore(current.getGScore()+tempConnection.getWeight()+cities.get(tempConnection.getDestination()));
					next.setFScore(next.getGScore() + heuristic.getCostEstimate(next, reqFlights, cities));

					frontier.add(next);
				}
			}
		}
		return;
	}

	/**
	 * showGraph method - this was used primarily for debugging.
	 */
	public void showGraph(){

		Collections.sort(connections);
		for(Connection c : connections){
			c.showConnection();
		}
		
		Collections.sort(reqFlights);
		for(Connection r : reqFlights){
			r.showConnection();
		}
	}
	
	/**getConnections
	 * Return a clone of the connections Arraylist
	 * @return
	 * 		returns a clone of the connections ArrayList
	 */		
	public ArrayList<Connection> getconnectionsClone(){
	    ArrayList<Connection> connectionsClone = new ArrayList<Connection>(connections.size());
	    for(Connection element : connections){
	    	Connection connectionClone = (Connection) element.clone();
	    	connectionsClone.add(connectionClone);
	    }

	    return connectionsClone;
	}
		
	/**
	 * @param origin
	 * 		String of origin city
	 * @param destination
	 * 		String of destination city
	 * @return
	 * 		returns the weight of the connection
	 */
	public int getConnectionWeight(String origin, String destination){
		Connection tempConnection = new Connection(origin, destination);
		int weight = 0;
		
		for(Connection c : connections){
			if(c.equals(tempConnection)){
				System.out.println("getWeight(): connection found");
				weight = c.getWeight();
				break;
			}
		}
		
		return weight;
	}
	
	// private variables
	private Hashtable<String, Integer> cities;
	private ArrayList<Connection> connections;
	private ArrayList<Connection> reqFlights;

}
