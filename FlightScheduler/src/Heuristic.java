import java.util.ArrayList;
import java.util.Hashtable;

public class Heuristic implements HeuristicInterface {

	/*
	 this is a fairly straight forward heuristic. (since the heuristic must be efficient, or it defeats the purpose
	 of having the heuristic in the first place. 
	 this heuristic simply adds the weight of any required flights if they have not yet been visited/(flown), and subtracts
	 the weight of the edge if it has already been flown.
	 States that have made more 'progress' toward the end goal (ie, they have more required flights than others) 
	 should be pushed toward the front of the priorityQueue.
	
	
	 for reference, some of the other heuristics that I wrote are given below - the idea is approximately the same,
	 if a required flight has not already been made, we increase its hScore, to push it further back in the queue
	
	 Additionally, I had the following code line in my else condition from below.
	 it would add the delay weight of the city nodes
	 I believe however that this breaks the requirements for admissibility (if by just a little)
	 and is therefore omitted from the solution.
	 It breaks admissibility since by adding the weight of the city delay values, we may
	 add a delay value that belongs to the destination city (which is not to be counted, 
	 or the origin city (which is again, not to be counted). 
	 and would therefore be an overestimation, which would break the admissibility requirement
	 hScore += newCities.get(req.getOrigin()) + newCities.get(req.getDestination());
	 However, I believe it is ok to subtract this amount from an hScore if the flight has already been made - 
	 this should not violate admissibility
	*/
	
	@Override
	public int getCostEstimate(State successor, ArrayList<Connection> reqFlights, Hashtable<String, Integer> newCities) {
		int hScore = 0;
		ArrayList<Connection> visited = successor.getVisitedClone();
		for(Connection req : reqFlights){
			// NOTE: each of the following options were trialled separately, and the result was equal. (ie, choosing one at a time yields the same result)
			// in concert they improve the performance of the heuristic with very little increase in computational complexity.
			// if our state does not yet contain the required flight connection, we add the cost of that connection to the hScore.
			if(!visited.contains(req)){
			    hScore += req.getWeight();
			    //hScore += newCities.get(req.getOrigin()) + newCities.get(req.getDestination());
			    
			// similarly, if the current state already contains that flight, we subtract the cost of that flight from the hScore
			}else{
			    hScore -= req.getWeight();
			    hScore -= newCities.get(req.getOrigin()) + newCities.get(req.getDestination());
			}
			
		}
		return hScore;
	}
}

///////////////////////////////////////////////////////////////////
// 					OLD HEURISTICS FOR REFERENCE				 //
///////////////////////////////////////////////////////////////////

/*
	@Override
	public int getCostEstimate(State successor, ArrayList<Connection> connections,  ArrayList<Connection> newRequiredFlights, Hashtable<String, Integer> newCities) {
		int sum = getRemainingDistance(successor.getVisitedClone(), newRequiredFlights, newCities); 
				+ getClosestFlight(successor, connections, newRequiredFlights); 
		sum -= newCities.get(successor.getCurrentCity());
		System.out.println("h(x): "+sum);
		return sum;
	}
	
	// need to modify - potential double counting of source/destination nodes here
	private int getRemainingDistance(ArrayList<Connection> visited,  ArrayList<Connection> reqFlights, Hashtable <String, Integer> cities){
		int sum = 0;
		
		for (Connection c : reqFlights){
			if(!visited.contains(c)){
				sum += (c.getWeight() + cities.get(c.getOrigin()) + cities.get(c.getDestination()));
			}
		}
		//System.out.println("Sum: "+sum);
		return sum;
	}
	
	// this is bad. NEED TO FIX (N^2)
	private int getClosestFlight(State successor, ArrayList<Connection> connections, ArrayList<Connection> reqFlights){
		
		int minDistance = Integer.MAX_VALUE;
		for(Connection required: reqFlights){
			for(Connection connection: connections){
				if(connection.getOrigin().equals(successor.getCurrentCity()) && connection.getDestination().equals(required.getOrigin())){
					if(connection.getWeight() < minDistance){
						minDistance = connection.getWeight();
					}
				}
			}
		}
		
		
		//System.out.println("minDist:"+minDistance);
		return minDistance;
	}
	

	
	ZERO heuristic
	@Override
		public int getCostEstimate(State successor) {
			return 0;
		}
*/