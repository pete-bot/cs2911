/**
 * @author Peter Kydd. 
 * z3367463
 *	This class defines the connection type - a type that contains the origin and destination string as well as the weight
 *  of that connection (edge)
 */
public class Connection implements Comparable<Connection>, Cloneable{
	
	private String source;		// source city
	private String destination; //
	int weight;			// weight of connection from source city to this city
	
	
	/**
	 * Class constructor 
	 * @param source
	 * 		String representing the origin city.
	 * @param destination
	 * 		String representing the destination city.
	 * @param newWeight
	 * 		Int representing the weight of the city.
	 */
	public Connection(String source, String destination, int newWeight ){
		//System.out.println("source: "+source+", destination: "+destination);
		this.source = source;
		this.destination = destination;
		//System.out.println("source: "+this.source+", destination: "+this.destination);
		this.weight = newWeight;
	}
	
	
	/**
	 * Alternate Constructor Class (no weight - used for comparisons)
	 * @param source
	 * 		String -name of the origin
	 * @param destination
	 * 		String - name of the destination
	 */		
	public Connection(String source, String destination ){
		//System.out.println("source: "+source+", destination: "+destination);
		this.source = source;
		this.destination = destination;
		//System.out.println("source: "+this.source+", destination: "+this.destination);
	}
	
	
	// return point A of this edge
	/**
	 * @return 
	 * 		returns the origin String
	 */		
	public String getOrigin(){
		return this.source;
	}
	
	

	/**
	 * @return
	 * 		returns the destination String
	 */
	public String getDestination(){
		return this.destination;
	}
	
	
	// shows the connection
	/**
	 * Shows the connection with weight - used for debug purposes
	 */
	public void showConnection(){
		System.out.print("Flight "+source+" to "+destination);
		System.out.println();
	}
	
	
	/**
	 * Shows the connection with no weight - used for debug purposes
	 */
	public void showConnectionNoWeight(){
		System.out.print(source+" ->"+destination+"\t");
		//System.out.println();
	}
	
	
	
	
	/**
	 * returns the weight of a connection
	 * @return	
	 * 		the weight of the connection
	 */
	public int getWeight(){
		return this.weight;
	}

	
	
	/* (non-Javadoc)
	 * A modified compareTo method
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Connection o) {
		int returnVal = 0;
		if( (o.getOrigin().equals(source)) && (o.getDestination().equals(destination) )){
			//System.out.println("dest: "+destination+" newDest: "+o.getDestination());
			returnVal = 1;
		}
		return returnVal;
	}
	
	
	/* (non-Javadoc)
	 * a modified equals method.
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
	    if (obj == null) {
	        return false;
	    }
	    
	    final Connection other = (Connection) obj;
	    if (!(this.getOrigin().equals(other.getOrigin())) || !(this.getDestination().equals(other.getDestination()))) {
	    	return false;
	    }
	    
	    return true;
	    
	}
	
	
	/* (non-Javadoc)
	 * implements clone
	 * @see java.lang.Object#clone()
	 */
	public Object clone(){  
	    try{  
	        return super.clone();  
	    }catch(Exception e){ 
	        return null; 
	    }
	}
	
	
}
	