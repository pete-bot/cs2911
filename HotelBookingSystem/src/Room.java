// A class that allows the creation of a simple date system
// this date system will be used to keep track of the duration of a booking


public class Room {

	//public static int roomCapacity = 1;
	public int number;
	public int capacity;
	
	/**
	 * Room class constructor
	 * @param newNumber
	 * 		number of the room. ex: room 103
	 * @param capacity
	 * 		capacity (type) of the room. 1, 2 or 3. (single, double, triple)
	 * 
	 * preconditions:
	 * 		-capacity must be either 1, 2 or 3.
	 */
	public Room(int newNumber, int capacity){
		date = new SimpleDate();
		this.number = newNumber;
		this.capacity = capacity;
	}
	
	/**
	 * Method to book a room for the specified time period at the specified date.
	 * @param month
	 * 		Month the booking starts in.
	 * @param day
	 * 		Day of month that booking starts.
	 * @param duration
	 * 		Duration of booking.
	 * @param bookingID
	 * 		Booking ID number. Stored in bookings array to keep track of bookings.
	 * @return
	 * 		returns the booked room number - this is so we can keep track of which rooms have been booked out.
	 * 
	 * preconditions:
	 * 		- month string must be in the correct format (Jan, Feb etc.)
	 * 		-day >=0 , day<=31 (depending on month)
	 * 		-duration>=0, duration<=365
	 */
	public int bookRoom(String month, int day, int duration, int bookingID){
		date.addBooking(month, day, duration, bookingID);
		return this.number;
	}
	
	
	/**
	 * A function that checks to see if a room is available before booking it. This is necessary to make sure that a room is completely available
	 * and that the remaining rooms in a booking request can be booked BEFORE booking any of them.
	 * @param month
	 * 		Month of booking
	 * @param day
	 * 		Day of month of booking
	 * @param duration
	 * 		Duration of booking
	 * @return
	 * 		Returns the room number if the room is available. In the Hotel class, the room number is then added to an array list for booking at a later process.
	 * 
	 * preconditions:
	 * 		- month string must be in the correct format (Jan, Feb etc.)
	 * 		-day >=0 , day<=31 (depending on month)
	 * 		-duration>=0, duration<=365
	 */
	public int isAvailable(String month, int day, int duration){
		int roomNum = 0;
	
		if ( date.isAvailable(month, day, duration ) ){
			roomNum = this.number;	
		}
		
		return roomNum;
	}
	
	
	/**
	 * This function removes a booking from the bookings array of a room. 
	 * @param bookingID
	 * 		The ID of the booking to be removed.
	 * @return
	 * 		Returns if the bookng was successfully deleted.
	 * 
	 * preconditions:
	 * 		- the booking must be in the system already 
	 */
	public boolean deleteBooking(int bookingID){
		boolean isDeleted = false;
		isDeleted = date.deleteBooking(bookingID);
		return isDeleted;
	}
	
	/**
	 * A function for debug printing the bookings of the year by month.
	 */
	public void printYearSchedule(){
		date.printYearSchedule();
	}
	
	/**
	 * A function that returns the capacity of the room (single, double, triple) as an int.
	 * @return
	 * 		The capacity of the room in int form (single = 1, double = 2, triple = 3).
	 */
	public int getCapacity(){
		return this.capacity;
	}
	
	/**
	 * Returns the room number
	 * @return
	 * 		Returns the room number.
	 */
	public int getRoom(){
		return this.number;
	}
	
	/**
	 * Method to handle the print instruction from user. 
	 */
	public void printBookings(){
		date.printBookings();
		
	}
	
	// date object that we store the room booking schedule in.
	private SimpleDate date;

	
}
