// A class that provides a simple date format (simpler to use than calendar) 
// in order to determine th booking shcedule for rooms

import java.util.*;

public class SimpleDate {
	
	/**
	 * SimpleDate class constructor
	 * Contains bookings array and 
	 */
	public SimpleDate(){
		
		Arrays.fill(bookings, 0);
		
		Arrays.fill(months, 0, 31,    "Jan");
		Arrays.fill(months, 31, 59,   "Feb");
		Arrays.fill(months, 59, 90,   "Mar");
		Arrays.fill(months, 90, 120,  "Apr");
		Arrays.fill(months, 120, 151, "May");
		Arrays.fill(months, 151, 181, "Jun");
		Arrays.fill(months, 181, 212, "Jul");
		Arrays.fill(months, 212, 243, "Aug");
		Arrays.fill(months, 243, 273, "Sep");
		Arrays.fill(months, 273, 304, "Oct");
		Arrays.fill(months, 304, 334, "Nov");
		Arrays.fill(months, 334, 365, "Dec");

		// for some reason, initialising here would create run time errors. not sure why. 
		//monthMap = new HashMap < String, Integer >();
		
		monthMap.put("Jan", 0);
		monthMap.put("Feb", 31);
		monthMap.put("Mar", 59);
		monthMap.put("Apr", 90);
		monthMap.put("May", 120);
		monthMap.put("Jun", 151);
		monthMap.put("Jul", 181);
		monthMap.put("Aug", 212);
		monthMap.put("Sep", 243);
		monthMap.put("Oct", 273);
		monthMap.put("Nov", 304);
		monthMap.put("Dec", 334);
	}
	
	/**
	 * Method to add booking to the booking array
	 * @param month
	 * 		Month of booking.
	 * @param day
	 * 		Day of month of booking.
	 * @param duration
	 * 		Duration of booking
	 * @param bookingID
	 * 		Id number of booking
	 */
	public void addBooking(String month, int day, int duration, int bookingID){
		// correct for ordinal/cardinal counting
		
		day = day-1;
		int startDate = monthMap.get(month) + day;
		int endDate = monthMap.get(month) + day + duration;
		
		if(endDate <=365){
			for(int count = startDate; count < endDate; count++){
				bookings[count] = bookingID;
			}
		}else{
			System.out.println("Duration is not in range for 2015!");
		}
	}
	
	
	/**
	 * Method to delete a booking
	 * @param bookingID
	 * 		Booking ID to delete from booking array.
	 * @return
	 * 		returns true if a booking has been deleted.
	 */
	public boolean deleteBooking(int bookingID){
		boolean isDeleted = false;
		int count = 0;
		
		while( (count < bookings.length) && (isDeleted == false) ){
			// case that we are at the end of the array
			if( (bookings[count] == bookingID) && ( count == (bookings.length-1) ) ){
				bookings[count] = 0;
				isDeleted = true;
			// case where previous and current day are booked, 
			}else if( (bookings[count] == bookingID) && (bookings[count+1] == bookingID) ) {
				bookings[count] = 0;
			}else if( (bookings[count] == bookingID) && (bookings[count+1] != bookingID) ){
				bookings[count] = 0;
				isDeleted = true;
			}
			count++;
		}
		return isDeleted;
	}

	
	/**
	 * A method to check if a room is available on the specified dates. Used to check room availability before 
	 * committing to a booking
	 * @param month
	 * 		Month of booking
	 * @param day
	 * 		Day of month of booking
	 * @param duration
	 * 		Duration of booking
	 * @return
	 * 		returns true if the dates are available for a given room, false otherwise.
	 * 
	 * preconditions:
	 * 		- month string must be in the correct format (Jan, Feb etc.)
	 * 		-day >=0 , day<=31 (depending on month)
	 * 		-duration>=0, duration<=365
	 */
	public boolean isAvailable(String month, int day, int duration){
		// account for indexation starting at 0 rather than 1.
		day = day-1;
		boolean isFree = true;
		int counter = day + monthMap.get(month);
		int endDate = counter + duration;
		
		while( counter < endDate){
			if(bookings[counter] != 0){
				isFree = false;
				break;
			}
			counter++;
		}
		return isFree;
	}
	
	/**
	 * A function that, called on a room will return the booking ID associated with a certain date.
	 * @param month
	 * 		the month to check
	 * @param day
	 * 		the day of the month
	 * @return
	 * 		the booking number on the date specified
	 * * 
	 * preconditions:
	 * 		- month string must be in the correct format (Jan, Feb etc.)
	 * 		-day >=0 , day<=31 (depending on month)
	 * 
	 */
	public int getCurrentBooking(String month, int day){
		int currentDate = monthMap.get(month) + day;
		return bookings[currentDate];
	}
	
	/**
	 * Method to return the int value of a month (jan = 1, Feb = 2 ..)
	 * @param month
	 * 		Input month
	 * @return
	 * 		Returns the integer value of the month (January = 1 etc..).
	 * preconditions:
	 * 		- month string must be in the correct format (Jan, Feb etc.)
	 */
	public int getMonth(String month){
		return monthMap.get(month);
	}
	
	
	/**
	 * A method to print out the entire contents of the booking array for a room.
	 * used for debugging
	 */
	public void printYearSchedule(){
		
		for(int count = 0; count < bookings.length; count++){
			if(count == monthMap.get("Jan")){
				System.out.println("January:");	
			}else if(count == monthMap.get("Feb")){
				System.out.println("\nFebruary:");
			}else if(count == monthMap.get("Mar")){
				System.out.println("\nMarch:");
			}else if(count == monthMap.get("Apr")){
				System.out.println("\nApril:");
			}else if(count == monthMap.get("May")){
				System.out.println("\nMay:");
			}else if(count == monthMap.get("Jun")){
				System.out.println("\nJune:");
			}else if(count == monthMap.get("Jul")){
				System.out.println("\nJuly:");
			}else if(count == monthMap.get("Aug")){
				System.out.println("\nAugust:");
			}else if(count == monthMap.get("Sep")){
				System.out.println("\nSeptember:");
			}else if(count == monthMap.get("Oct")){
				System.out.println("\nOctober:");
			}else if(count == monthMap.get("Nov")){
				System.out.println("\nNovember:");
			}else if(count == monthMap.get("Dec")){
				System.out.println("\nDecember:");
			}
			
			System.out.print(bookings[count]+" ");
			
			
		}
		System.out.println("");
	}
	
	/**
	* The official print bookings function. This is called on each room when the PRINT instruction is issued by the user.
	*/
	public void printBookings(){
	    int current = 0;
	    int previous = 0;
		
		int duration = 0;
		String monthValue = "";
		int dayValue = 0;
		
	    for(int count = 0; count < bookings.length; count++){
	    	current = bookings[count];
	    	
	    	// booking state change
	    	if( current != previous ){
	    		
	    		// 0 -> booking
	    		if( (previous == 0) && (current != 0) ){
	    			monthValue = months[count];
	    			dayValue = count +1 - monthMap.get(months[count]);
	    		}else if( (previous != 0) && (current != 0) ){
	    			System.out.print(" " + monthValue+" "+ dayValue +" "+ duration);
	    			dayValue = count +1 - monthMap.get(months[count]);
	    			monthValue = months[count]; 
	    			
	    		// booking -> 0
	    		}else if( (previous != 0) && (current == 0)){
	    			System.out.print(" " +monthValue+" "+ dayValue +" "+ duration);
	    		}
	    		duration = 0;
				previous = current;			
	    	
	    	}else if(count == (bookings.length -1)){

	    		if(bookings[count] != 0){
	    			duration++;
	    			System.out.print(" " + monthValue+" "+ dayValue +" "+ duration);
	    		}
	    	}
	    	duration++;
		}
	}
	
	private Integer[] bookings = new Integer[365];
	private String[] months = new String[365];
	
	// for some reason, initializing this in the constructor would upset the program at runtime.
	private Map<String, Integer> monthMap = new HashMap < String, Integer >();
}