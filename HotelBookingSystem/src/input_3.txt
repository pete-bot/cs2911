Hotel Geronimo 101 1
Hotel SittingBull 105 1
Hotel BlackHawk 110 1

// Ensure that the first hotel chosen is the first hotel 'created'
Booking 1 Jan 17 12 single 1
Booking 2 Feb 17 12 single 1
Booking 3 Mar 17 12 single 1
Booking 4 Apr 17 12 single 1
Booking 5 May 17 12 single 1
Booking 6 Jun 17 12 single 1
Booking 7 Jul 17 12 single 1
Booking 8 Aug 17 12 single 1
Booking 9 Sep 17 12 single 1
Booking 10 Oct 17 12 single 1
Booking 11 Nov 17 12 single 1
Booking 12 Dec 17 12 single 1

Print Geronimo
Print SittingBull
Print BlackHawk

// Ensure that month roll over of bookings working correctly.
// and that booking collsion is handled correctly (ie, assigned to another free hotel, 
// not rejected)
Booking 13 Jan 17 20 single 1
Booking 25 Jan 18 10 single 1
Booking 14 Feb 17 20 single 1
Booking 26 Feb 18 10 single 1

Booking 15 Mar 17 20 single 1
Booking 27 Mar 18 10 single 1

Booking 16 Apr 17 20 single 1
Booking 28 Apr 18 10 single 1

Booking 17 May 17 20 single 1
Booking 29 May 18 10 single 1

Booking 18 Jun 17 20 single 1
Booking 30 Jun 18 10 single 1

Booking 19 Jul 17 20 single 1
Booking 31 Jul 18 10 single 1

Booking 20 Aug 17 20 single 1
Booking 32 Aug 18 10 single 1

Booking 21 Sep 17 20 single 1
Booking 33 Sep 18 10 single 1

Booking 22 Oct 17 20 single 1
Booking 34 Oct 18 10 single 1

Booking 23 Nov 17 20 single 1
Booking 35 Nov 18 10 single 1

Booking 24 Dec 17 14 single 1
Booking 36 Dec 18 10 single 1

Print Geronimo
Print SittingBull
Print BlackHawk

