// Class that manages our hotel objects

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 
 * @author Peter Kydd
 * @author z3367463
 * 
 */


public class HotelManager {

	/**
	 * Constructor for Hotel class.
	 */
	public HotelManager(){
		HotelMap = Collections.synchronizedMap( new LinkedHashMap<String, Hotel>());
	}
	
	/**
	 * This is where we construct our hotels - we must add rooms here. 
	 * This function populates a hashMap to store the rooms.
	 * @param name
	 * 		name of hotel.
	 * @param roomNum
	 * 		room number.
	 * @param roomCap
	 * 		room capacity. 
	 * 
	 * preconditions:
	 * 		-roomNum >=0
	 * 		-roomCap == ( 1 || 2 || 3 )
	 * 		-name should be 
	 * 
	 * postconditions:
	 * 		-Number of hotels will increase by 1.
	 * 		-Room that has been passed in with Hotel will be created, whether or not the hotel exists already in the system. 
	 */
	public void addHotel(String name, int roomNum, int roomCap){
		
		// hotel is already in system
		if( HotelMap.get(name) != null){
			HotelMap.get(name).addRoom(roomNum, roomCap);
		
		// hotel is not yet in system
		}else{

			Hotel newHotel = new Hotel(name);
			HotelMap.put(name, newHotel);
			HotelMap.get(name).addRoom(roomNum, roomCap);	
		}
	}
	
	/**
	 * 
	 * A function that allows us to create bookings. It will iterate through the hotels until it finds a hotel with enough space to accept a complete booking.
	 * If a hotel cannot accommodate an entire booking, the booking will be rejected.
	 * 
	 * @param bookingID
	 * 		the booking number.
	 * @param month
	 * 		the month the booking begins.
	 * @param day
	 * 		the day that the booking begins.
	 * @param duration
	 * 		the duration of the stay.
	 * @param roomTypeOne
	 * 		the first type of room required. (ie, single, double, triple).
	 * @param numRoomTypeOne
	 * 		number of rooms of type one required.
	 * @param roomTypeTwo
	 * 		second type of room required.
	 * @param numRoomTypeTwo
	 * 		number of rooms of second room required.
	 * @param roomTypeThree
	 * 		third type of room required
	 * @param numRoomTypeThree
	 * 		number of third room required
	 * @param modType
	 * 		a value that allows us to determine if this is a new booking or a modification of a booking
	 * 		(used for print output)
	 * @return
	 * 		returns whether or not the booking was made. 
	 * 
	 * preconditions:
	 * 		-The addBookng method requires that the bookingID >= 0
	 *  	-the month string being passed in adheres to the standard 3 letter form, ex: Jan for january, Feb for february etc.
	 * 		-strings roomTypeOne, roomTypeTwo and roomTypeThree are of the form "single", "double" or "triple"
	 * 		-modType is of the type "Change" or "Booking"
	 * 		-duration>=0, duration <=365
	 * 		-day>=0, day<=31
	 * 
	 * postconditions
	 * 		-Number of bookings will increase by one, as long as the booking is able to be made (no bookng collisions)
	 * 		- no other bookings will be damaged in this process. 
	 * 
	 */
	public boolean addBooking(int bookingID, String month, int day, int duration, String roomTypeOne, int numRoomTypeOne, 
			String roomTypeTwo, int numRoomTypeTwo, String roomTypeThree, int numRoomTypeThree, String modType ){
		
		boolean bookingWasMade = false;
		
		int numRoomOne   = 0;
		int numRoomTwo   = 0;
		int numRoomThree = 0;
		
		// the next section allows us to organise the rooms in the correct order
		// Check first room type
		if(roomTypeOne.equals("single")){
			numRoomOne += numRoomTypeOne;
		}else if(roomTypeOne.equals("double")){
			numRoomTwo += numRoomTypeOne;
		}else if(roomTypeOne.equals("triple")) {
			numRoomThree += numRoomTypeOne;
		}
		
		// Check second room type
		if(roomTypeTwo.equals("single")){
			numRoomOne += numRoomTypeTwo;
		}else if(roomTypeTwo.equals("double")){
			numRoomTwo += numRoomTypeTwo;
		}else if(roomTypeTwo.equals("triple")){
			numRoomThree += numRoomTypeTwo;
		}
		
		// check the third type 
		if(roomTypeThree.equals("single")){
			numRoomOne += numRoomTypeThree;
		}else if(roomTypeThree.equals("double")){
			numRoomTwo += numRoomTypeThree;
		}else if(roomTypeThree.equals("triple")){
			numRoomThree += numRoomTypeThree;
		}
		
		for (Hotel value : HotelMap.values()) {
			bookingWasMade = value.bookRoom(month, day, duration, numRoomOne, numRoomTwo, numRoomThree, bookingID, value.getName(), modType);
			if(bookingWasMade){
				System.out.println("");
				break;
			}
		}
	
		return bookingWasMade;
	}
	
	/**
     * A function that allows us to delete bookings by booking ID.
	 * 
	 * @param bookingID
	 * 		the booking ID to be deleted
	 * @return
	 * 		whether or not the booking was successfully deleted
	 */
	public boolean deleteBooking(int bookingID){
		boolean isDeleted = false;
		
		for (Hotel value : HotelMap.values()) {
			isDeleted = value.deleteBooking(bookingID);

			if(isDeleted){
				break;
			}
		}
		
		return isDeleted;
	}
	
	/**
	 * A function that allows us to show the hotel days and bookings.
	 * For debugging purposes 	
	 */
	public void showHotels(){
		for (String key : HotelMap.keySet()) {
		    System.out.println("Hotel: " + key );
		    HotelMap.get(key).showHotel();
		}
		
	} 
	/**
	 * This function allows us to print the hotel bookings out by room number, then date.
	 * @param Hotel
	 * 
	 * preconditions:
	 * 		-The Hotel String argument should belong to a hotel that has been entered in the system. However, if this is not the case,
	 * 		the program should not break
	 * 
	 * postconditions:
	 * 		-This method will not later anything in the system, simply print out
	 * 		therefore, the system will work after this operation no matter what.
	 */
	public void printHotel(String Hotel){
		HotelMap.get(Hotel).printHotel(Hotel);
	}
	
	/**
	 * @param HotelMap:
	 * 		Hashmap of all of the associated hotels.
	 * 
	 */
	
	// our map of hotels
	private Map<String, Hotel > HotelMap;
	
}
