// Peter Kydd
// COMP2911 assignment 1
// this is the main function - it it will take in arguments from the command line 
// that will allow for the generation of a hotel booking system and the data within


import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Peter Kydd
 * @author z3367463
 * 
 *
 */

public class HotelBookingSystem {

	// Main function. Will take in the input file and parse for data
	// will populate a set of hotel objects
	public static void main (String argv[]){
		
		// take input from text file from argument
		try {
			
			Path filename =  Paths.get(argv[0]);
		
			Charset charset = Charset.forName("US-ASCII");
			
			try (BufferedReader reader = Files.newBufferedReader(filename, charset)) {
			    String line = null;
			    
			    // create a new hotel manager class
			    newHotelManager = new HotelManager();
			    
			    // parse file and extract tokens/instructions
			    while ((line = reader.readLine()) != null) {
			        
			    	/**
			    	 * HOTEL instruction handler
			    	 */
			    	if(line.matches("^Hotel .*")){
			    		
			    		//System.out.println(line);
			    		Pattern pattern = Pattern.compile("^Hotel\\s([a-zA-Z]*)\\s([0-9]*)\\s([0-9]*)");
			        	Matcher matcher = pattern.matcher(line);
			        	
			        	while(matcher.find()){
				        	String hotelName = matcher.group(1);
				        	int roomNumber = Integer.parseInt(matcher.group(2));
				        	int roomCap = Integer.parseInt(matcher.group(3));
				        	newHotelManager.addHotel(hotelName, roomNumber, roomCap);
				        }	
			        	
			        /**
			         * BOOKING instruction handler
			         */
			        } else if(line.matches("^Booking\\s.*")){
			        	
			        	boolean bookingWasMade = false;
			        	String[] argArray = line.split(" ");
			        	
			        	String modType = "Booking";
			        	
			        	// book one room
			        	if(argArray.length == 7){
							bookingWasMade = newHotelManager.addBooking(Integer.parseInt(argArray[1]), argArray[2],
									Integer.parseInt(argArray[3]), Integer.parseInt(argArray[4]), 
									argArray[5], Integer.parseInt(argArray[6]), 
									"NA", 0, "NA", 0, modType);
			        	
						// book two rooms
			        	}else if(argArray.length == 9){
			        		bookingWasMade = newHotelManager.addBooking(Integer.parseInt(argArray[1]), argArray[2],Integer.parseInt(argArray[3]),
									Integer.parseInt(argArray[4]), argArray[5],	Integer.parseInt(argArray[6]), argArray[7],	
									Integer.parseInt(argArray[8]), "NA", 0, modType);
				        
			        	// book three rooms
			        	}else if (argArray.length == 11){
			        		bookingWasMade = newHotelManager.addBooking(Integer.parseInt(argArray[1]), argArray[2],Integer.parseInt(argArray[3]),
									Integer.parseInt(argArray[4]), argArray[5],	Integer.parseInt(argArray[6]), 
									argArray[7],	Integer.parseInt(argArray[8]),argArray[9],	Integer.parseInt(argArray[10]), modType);	
			        	}
			        	
			        	if( !(bookingWasMade) ){
			        		System.out.println("Booking rejected");
			        	}
			        	
			   
			        	
			        /**
			         * CHANGE booking instruction handler
			         */
			        } else if(line.matches("^Change .*")){
			        	
			        	boolean isDeleted = false;
			        	boolean bookingWasMade = false;
			        	
			        	String modType = "Change";
			        	String[] argArray = line.split(" ");
			        	
			        	isDeleted = newHotelManager.deleteBooking(Integer.parseInt(argArray[1]));
			        	
			        	if(isDeleted){
				        	// book one room
				        	if(argArray.length == 7){
								bookingWasMade = newHotelManager.addBooking(Integer.parseInt(argArray[1]), argArray[2],
										Integer.parseInt(argArray[3]), Integer.parseInt(argArray[4]), 
										argArray[5], Integer.parseInt(argArray[6]), 
										"NA", 0, "NA", 0, modType);
				        	
							// book two rooms
				        	}else if(argArray.length == 9){
	
				        		bookingWasMade = newHotelManager.addBooking(Integer.parseInt(argArray[1]), argArray[2],Integer.parseInt(argArray[3]),
										Integer.parseInt(argArray[4]), argArray[5],	Integer.parseInt(argArray[6]), argArray[7],	
										Integer.parseInt(argArray[8]), "NA", 0, modType);
					        
				        	// book three rooms
				        	}else if (argArray.length == 11){
	
				        		bookingWasMade = newHotelManager.addBooking(Integer.parseInt(argArray[1]), argArray[2],Integer.parseInt(argArray[3]),
										Integer.parseInt(argArray[4]), argArray[5],	Integer.parseInt(argArray[6]), 
										argArray[7],	Integer.parseInt(argArray[8]),argArray[9],	Integer.parseInt(argArray[10]), modType);	
				        	}
				        	
				        	if( !(bookingWasMade) ){
				        		System.out.println("Booking rejected!");
				        	}
			        	}else if(!(isDeleted)){
			        		System.out.println("Change rejected");
			        	}
			        	
			        /**
			         * CANCEL instruction handler. 
			         */
			        } else if(line.matches("^Cancel .*")){
			        	
			        	String[] argArray = line.split(" ");
			        	boolean isDeleted = false;
			        	isDeleted = newHotelManager.deleteBooking(Integer.parseInt(argArray[1]));
			        	
			        	if(isDeleted){
			        		System.out.println(line);
			        	}else{
			        		System.out.println("Cancel rejected");
			        	}
			        
			        /**
			         * PRINT instruction handler.
			         */
			        } else if(line.matches("^Print .*")){
			        	String[] argArray = line.split(" ");
			        	
			        	newHotelManager.printHotel(argArray[1]);
			       	
			        /**
			         * UKNOWN instruction handler (instruction does not fall into previous categories)
			         */
			        } else {
			        	//System.out.println("UNKNOWN INSTRUCTION FOUND: "+ line);
			        }   
			    }			
			
			} catch (IOException x) {
			    System.err.format("IOException: %s%n", x);
			}
			
		} catch (Exception e){
			System.out.println("No arguments found.");
			System.out.println(e.getStackTrace());
		}
	}
	// hotel object
	private static HotelManager newHotelManager; 

}
