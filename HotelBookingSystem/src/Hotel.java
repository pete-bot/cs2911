// Hotel class is responsible for all the hotel related data and methods that are used for the system

// TODO
// need to interact with our new hash map
// create new entry into hashmap
// modify entry
// print entry 

import java.util.*;


public class Hotel {
	
	public String name;
	
	/**
	* The class constructor for Hotel class.
	* @param newName
	*		the name of the hotel. ex: Surfers
	*/
	public Hotel(String newName){
		
		this.name = newName;
		RoomMap = Collections.synchronizedMap( new LinkedHashMap<Integer, Room>());
	}
	
	/**
	* This is the method that controls the booking of rooms. 
	* @param month
	* 		The month in which the booking starts
	* @param day
	* 		Day of the month in which the booking starts
	* @param duration
	* 		Duration of the booking. 
	* @param cap1
	* 		Number of single rooms required
	* @param cap2
	* 		Number of double rooms required
	* @param cap3
	* 		Number of triple rooms required
	* @param bookingID
	* 		ID number of the booking. This number is stored in the date array in the room object
	* @param hotelName
	* 		The name of the hotel. This is used in the print function
	* @param modType
	* 		The type of operation we are conducting, new booking or changing an existing booking. Used in the print instruction
	* @return
	* 		Returns true when a booking is made so that we know that we do not have to attempt any further booking for this particular booking ID
	* 
	* 
	* preconditions:
	* 		-month String must be of 3 letter standard type: Jan, Feb for January, February etc.
	* 		-day>=0, day<=31
	* 		-duration>=0, duration <=365
	* 		-cap1, cap2, cap3 >=0
	* 		-modType == "Change" or "Booking"
	* 
	* postconditions:
	* 		-provided that the room is available, the number of bookings will be increased by 1.
	* 		-if the booking is not available, the program will alert the user to the rejection and the booking will not be made.
	* 		-rooms will be booked in the order that the rooms were entered in the system initially. (see test 7 and 8)
	*/
	
	public boolean bookRoom(String month, int day, int duration, int cap1, int cap2, int cap3, int bookingID, String hotelName, String modType){
		
		boolean bookingMade = false;
		
		int capSingle = 1;
		int capDouble = 2;
		int capTriple = 3;
		
		int numSingles = 0;
		int numDoubles = 0;
		int numTriples = 0;
		
		ArrayList<Integer> vacancies = new ArrayList<Integer>();
	
		// check to determine if the rooms of each type (single, double, triple) are available.
		// if a room is available for a certain booking, it is added to the appropriate ArrayList initialised above. (in the order of room creation)
		// then, if there are enough of each room type available to satisfy the booking, the booking as made below
		for (Room value : RoomMap.values()) {
			
			// go through rooms and find rooms that are available at the required times and of the correct capacities.
			// if a room that satisfies the criteria exists, the room is pushed onto the vacancies list. 			
			if( ( value.isAvailable(month, day, duration ) != 0 ) ){
		    	vacancies.add(value.isAvailable(month, day, duration ));
		    	if(value.getCapacity() == capSingle ){
		    		numSingles++;
		    	}else if(value.getCapacity() == capDouble ){
		    		numDoubles++;
		    	}else if(value.getCapacity() == capTriple ){
		    		numTriples++;
		    	}
		    }			  	
		} 
		
		// check to see if enough of each type of room is available to satisfy the booking requirement.
		if( ( numSingles >= cap1 ) && ( numDoubles >= cap2 ) && ( numTriples >= cap3 ) ){
			int bookedRoomNumber = 0;

			int bookedSingles = 0;
			int bookedDoubles = 0;
			int bookedTriples = 0;
			
			// printing type
			if(modType.equals("Booking")){
				System.out.print("Booking "+bookingID+" "+hotelName);
			}else if(modType.equals("Change")){
				System.out.print("Change "+bookingID+" "+hotelName);
			}
			
			// iterate through the vacancies list and assign bookings to the first rooms in the list (making sure that they are assigned bookings in 
			// the order that they were created)
			while( !(vacancies.isEmpty()) ) {
				
				// if single
				if( (((RoomMap.get(vacancies.get(0))).getCapacity()) == capSingle) && (bookedSingles < cap1) ){
					bookedRoomNumber = RoomMap.get(vacancies.remove(0)).bookRoom(month, day, duration, bookingID);
					System.out.print(" " + bookedRoomNumber);
					bookedSingles++;
					
				// if double
				}else if( (((RoomMap.get(vacancies.get(0))).getCapacity()) == capDouble) && (bookedDoubles < cap2) ){
					bookedRoomNumber = RoomMap.get(vacancies.remove(0)).bookRoom(month, day, duration, bookingID);
					System.out.print(" " + bookedRoomNumber);
					bookedDoubles++;
					
				// if triple
				}else if( (((RoomMap.get(vacancies.get(0))).getCapacity()) == capTriple) && (bookedTriples < cap3) ){
					bookedRoomNumber = RoomMap.get(vacancies.remove(0)).bookRoom(month, day, duration, bookingID);
					System.out.print(" " + bookedRoomNumber );
					bookedTriples++;
				}else{
					vacancies.remove(0);
				}	
			}
			bookingMade = true;
		}
		
		
		return bookingMade;
	
	}
	
	/**
	 * This function adds rooms to the hotel as it is 'constructed'
	 * We do this to keep track of available rooms.
	 * @param roomNumber
	 * 		the number of the room. ex: room 103, 104 etc.
	 * @param capacity
	 * 		the number of spaces available in a room. there are three capacities available: single, double and triple
	 * 
	 * preconditions:
	 * 		-capacity integer is either 1, 2 or 3.
	 * 
	 * postconditions:
	 * 		-room is added to current hotel
	 */
	public void addRoom(int roomNumber, int capacity){		
			Room newRoom = new Room(roomNumber, capacity);
			RoomMap.put(roomNumber, newRoom);
	
	}
	/**
	 * This method deleted the booking associated with a bookingID
	 * @param bookingID
	 * 		the booking ID with which the associated booking is to be deleted
	 * @return
	 * 		returns whether the deletion of the booking was successful or not. This is used to 'early exit' the booking process
	 * 
	 * preconditions:
	 * 		-bookingID exists in the system. If this is not the case, then the cancellation will be rejected.
	 * 
	 * postconditions:
	 * 		-provided that the bookingID to be deleted exists, then the booking will be removed and the rooms will be freed up to 
	 * 		be used again
	 */
	public boolean deleteBooking(int bookingID){
		boolean isDeleted = false;
		ArrayList<Boolean> deletedBookings = new ArrayList<Boolean>();
		
		for (Room value : RoomMap.values()) {
		    isDeleted = value.deleteBooking(bookingID);	  	
		    if(isDeleted == true){
		    	deletedBookings.add(isDeleted);
		    }
		}
		
		// check to see if elements were pushed into deletedBookings 
		// if there are elements there, it means that bookings were deleted
		// since the bookings must be at the same hotel, then we have deleted all the bookings
		if(!(deletedBookings.isEmpty())){
			isDeleted = true;
		}
		
		return isDeleted;
	}
	
	/**
	 * This is a debug function for printing out all the rooms in a given hotel
	 */
	public void showHotel(){
		for (Room value : RoomMap.values()) {
		    System.out.println(value.getRoom());
		}
	}
	
	/**
	 * This function returns the name of the current hotel. Used for printing etc. 
	 * @return
	 * 		returns the name of the current hotel
	 */
	public String getName(){
		return this.name;
	}
	
	
	/**
	 * this prints out the entire year and shows all bookings
	 * used for debugging
	 */
	public void printYear(){
		for (Room value : RoomMap.values()) {
			value.printYearSchedule();
		}		
	}
	
	/**
	 * This is the method that is called when the print command is given by the user.
	 * @param Hotel
	 * 		this is the hotel name. Passed in and used for printing the name during the print call.
	 * 
	 * preconditions:
	 * 		-Hotel string must be a valid hotel name in the system. Otherwise there is no output.
	 */
	public void printHotel(String Hotel){
		for (Room value : RoomMap.values()) {
		    System.out.print(Hotel + " " + value.getRoom());
			value.printBookings();
		    System.out.println("");
		}
	}
	
	// room object hashmap
	private Map< Integer, Room > RoomMap;
	
}
